package com.ddm.product.controller;

import static com.ddm.product.utils.JsonUtility.asJsonString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import com.ddm.product.BpTransactionServiceApplication;
import com.ddm.product.enums.ResponseType;
import com.ddm.product.exception.ResourceNotFoundException;
import com.ddm.product.service.TransactionService;
import com.ddm.product.utils.TransactionInformationTestUtils;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = BpTransactionServiceApplication.class)
public class TransactionInformationControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private TransactionService transactionService;
	
	@Test
	@WithMockUser
	void getAllTransactionSuccessTest() throws Exception {
		
		given(transactionService.getAllTransactions())
				.willReturn(TransactionInformationTestUtils.formTransactionDataResponseListDto())
				.willReturn(Collections.emptyList());
		
		mockMvc.perform(get("/transaction/record")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result", hasSize(3)))
			      .andExpect(jsonPath("$.result[0].type", 
			    		  is(TransactionInformationTestUtils.formIncomeTransactionDataResponseDto().getType().name())))
			      .andExpect(jsonPath("$.result[0].transactionDate", 
			    		  is("01-Jan-2021")));

		mockMvc.perform(get("/transaction/record")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result", hasSize(0)));		
	}
	
	@Test
	void getAllTransactionUnAuthorizedAccessTest() throws Exception {
		
		mockMvc.perform(get("/transaction/record")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}
	
	@Test
	@WithMockUser
	void getTransactionByIdSuccessTest() throws Exception {
		
		given(transactionService.getTransactionById(1l))
				.willReturn(TransactionInformationTestUtils.formExpenseTransactionDataResponseDto())
				.willThrow(ResourceNotFoundException.class);
		
		mockMvc.perform(get("/transaction/1/record")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result.type", 
			    		  is(TransactionInformationTestUtils.formExpenseTransactionDataResponseDto().getType().name())))
			      .andExpect(jsonPath("$.result.transactionDate", 
			    		  is("02-Jan-2021")));
		
		mockMvc.perform(get("/transaction/1/record")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isNotFound())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.NOT_FOUND.name())));
	}
	
	@Test
	void getTransactionByIdUnAuthorizedAccessTest() throws Exception {
				
		mockMvc.perform(get("/transaction/1/record")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}
	
	@Test
	@WithMockUser
	void addTransactionSuccessTest() throws Exception {
		
		given(transactionService.addTransaction(TransactionInformationTestUtils.formIncomeTransactionDataAddRequestDto()))
				.willReturn(TransactionInformationTestUtils.formIncomeTransactionDataResponseDto());
		
		mockMvc.perform(post("/transaction/record")
				  .content(asJsonString(TransactionInformationTestUtils.formIncomeTransactionDataAddRequestDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isCreated())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result.id", is(notNullValue())));
	}
	
	@Test
	void addTransactionUnAuthorizedAccessTest() throws Exception {
		
		mockMvc.perform(post("/transaction/record")
				  .content(asJsonString(TransactionInformationTestUtils.formIncomeTransactionDataAddRequestDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}
	
	@Test
	@WithMockUser
	void updateTransactionSuccessTest() throws Exception {
		
		given(transactionService.updateTransaction(TransactionInformationTestUtils.formIncomeTransactionDataUpdateRequestDto()))
				.willReturn(TransactionInformationTestUtils.formIncomeTransactionDataResponseDto())
				.willThrow(ResourceNotFoundException.class);
		
		mockMvc.perform(put("/transaction/record")
				  .content(asJsonString(TransactionInformationTestUtils.formIncomeTransactionDataUpdateRequestDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result.id", is(notNullValue())));
		
		mockMvc.perform(put("/transaction/record")
				  .content(asJsonString(TransactionInformationTestUtils.formIncomeTransactionDataUpdateRequestDto()))
			      .contentType(MediaType.APPLICATION_JSON))
				  .andExpect(status().isNotFound())
				  .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
				  .andExpect(jsonPath("$.result.status", is(HttpStatus.NOT_FOUND.name())));
	}
	
	@Test
	void updateTransactionUnAuthorizedAccessTest() throws Exception {
		
		mockMvc.perform(put("/transaction/record")
				  .content(asJsonString(TransactionInformationTestUtils.formIncomeTransactionDataUpdateRequestDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}
	
	@Test
	@WithMockUser
	void updateAccountInformationInvalidRequestDtoTest() throws Exception {
		
		mockMvc.perform(put("/transaction/record")
				  .content(asJsonString(TransactionInformationTestUtils.formTransactionDataUpdateInvalidRequestDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isBadRequest())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.BAD_REQUEST.name())))
			      .andExpect(jsonPath("$.result.errors", hasSize(5)));	
	}
	
	@Test
	@WithMockUser
	void deleteTransactionByIdSuccessTest() throws Exception {
		
		given(transactionService.deleteTransaction(1l))
				.willReturn(Boolean.TRUE)
				.willThrow(ResourceNotFoundException.class);
		
		mockMvc.perform(delete("/transaction/1/record")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result", is(Boolean.TRUE)));
		
		mockMvc.perform(delete("/transaction/1/record")
			      .contentType(MediaType.APPLICATION_JSON))
				  .andExpect(status().isNotFound())
				  .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
				  .andExpect(jsonPath("$.result.status", is(HttpStatus.NOT_FOUND.name())));
	}
	
	@Test
	void deleteTransactionByIdUnAuthorizedAccessTest() throws Exception {
		
		mockMvc.perform(delete("/transaction/1/detail")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}

}
