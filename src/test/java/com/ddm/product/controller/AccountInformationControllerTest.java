package com.ddm.product.controller;

import static com.ddm.product.utils.JsonUtility.asJsonString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import com.ddm.product.BpTransactionServiceApplication;
import com.ddm.product.enums.ResponseType;
import com.ddm.product.exception.ResourceNotFoundException;
import com.ddm.product.service.AccountInfoService;
import com.ddm.product.utils.AccountInformationTestUtils;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = BpTransactionServiceApplication.class)
public class AccountInformationControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AccountInfoService accountInfoService;

	@Test
	@WithMockUser
	void getAllAccountInformationSuccessTest() throws Exception {
		
		given(accountInfoService.getAllAccountInformation())
				.willReturn(AccountInformationTestUtils.formAccoutInfoResonseDtoList())
				.willReturn(Collections.emptyList());
		
		mockMvc.perform(get("/account/detail")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result", hasSize(1)))
			      .andExpect(jsonPath("$.result[0].accountAlias", 
			    		  is(AccountInformationTestUtils.formAccoutInfoResponseDto().getAccountAlias())));

		mockMvc.perform(get("/account/detail")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result", hasSize(0)));		
	}
	
	@Test
	void getAllAccountInformationUnAuthorizedAccessTest() throws Exception {
				
		mockMvc.perform(get("/account/detail")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}
	
	@Test
	@WithMockUser
	void getOneAccountInformationSuccessTest() throws Exception {
		
		given(accountInfoService.getAccountInformationById(1l))
				.willReturn(AccountInformationTestUtils.formAccoutInfoResponseDto())
				.willThrow(ResourceNotFoundException.class);
		
		mockMvc.perform(get("/account/1/detail")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result.accountAlias", 
			    		  is(AccountInformationTestUtils.formAccoutInfoResponseDto().getAccountAlias())));
		
		mockMvc.perform(get("/account/1/detail")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isNotFound())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.NOT_FOUND.name())));
	}
	
	@Test
	void getOneAccountInformationUnAuthorizedAccessTest() throws Exception {
		
		mockMvc.perform(get("/account/1/detail")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}
	
	@Test
	@WithMockUser
	void addAccountInformationSuccessTest() throws Exception {
		
		given(accountInfoService.addAccountInformation(AccountInformationTestUtils.formAccoutInfoRequestDto()))
				.willReturn(AccountInformationTestUtils.formAccoutInfoResponseDto());
		
		mockMvc.perform(post("/account/detail")
				  .content(asJsonString(AccountInformationTestUtils.formAccoutInfoRequestDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isCreated())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result.id", is(notNullValue())));
	}
	
	@Test
	void addAccountInformationUnAuthorizedAccessTest() throws Exception {
		
		mockMvc.perform(post("/account/detail")
				  .content(asJsonString(AccountInformationTestUtils.formAccoutInfoRequestDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}
	
	@Test
	@WithMockUser
	void updateAccountInformationSuccessTest() throws Exception {
		
		given(accountInfoService.updateAccountInformation(AccountInformationTestUtils.formAccoutInfoResponseDto()))
				.willReturn(AccountInformationTestUtils.formAccoutInfoResponseDto())
				.willThrow(ResourceNotFoundException.class);
		
		mockMvc.perform(put("/account/detail")
				  .content(asJsonString(AccountInformationTestUtils.formAccoutInfoResponseDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result.id", is(notNullValue())));
		
		mockMvc.perform(put("/account/detail")
				  .content(asJsonString(AccountInformationTestUtils.formAccoutInfoResponseDto()))
			      .contentType(MediaType.APPLICATION_JSON))
				  .andExpect(status().isNotFound())
				  .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
				  .andExpect(jsonPath("$.result.status", is(HttpStatus.NOT_FOUND.name())));
	}
	
	@Test
	void updateAccountInformationUnAuthorizedAccessTest() throws Exception {
		
		mockMvc.perform(put("/account/detail")
				  .content(asJsonString(AccountInformationTestUtils.formAccoutInfoResponseDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}
	
	@Test
	@WithMockUser
	void updateAccountInformationInvalidRequestDtoTest() throws Exception {
		
		mockMvc.perform(put("/account/detail")
				  .content(asJsonString(AccountInformationTestUtils.formAccoutInfoInvalidResponseDto()))
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isBadRequest())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.BAD_REQUEST.name())))
			      .andExpect(jsonPath("$.result.errors", hasSize(4)));	
	}
	
	@Test
	@WithMockUser
	void deleteAccountInformationSuccessTest() throws Exception {
		
		given(accountInfoService.deleteAccountInformation(1l))
				.willReturn(Boolean.TRUE)
				.willThrow(ResourceNotFoundException.class);
		
		mockMvc.perform(delete("/account/1/detail")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.SUCCESS.name())))
			      .andExpect(jsonPath("$.result", is(Boolean.TRUE)));
		
		mockMvc.perform(delete("/account/1/detail")
			      .contentType(MediaType.APPLICATION_JSON))
				  .andExpect(status().isNotFound())
				  .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
				  .andExpect(jsonPath("$.result.status", is(HttpStatus.NOT_FOUND.name())));
	}
	
	@Test
	void deleteAccountInformationUnAuthorizedAccessTest() throws Exception {
		
		mockMvc.perform(delete("/account/1/detail")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnauthorized())
			      .andExpect(jsonPath("$.responseType", is(ResponseType.FAILURE.name())))
			      .andExpect(jsonPath("$.result.status", is(HttpStatus.UNAUTHORIZED.name())));	
	}

}
