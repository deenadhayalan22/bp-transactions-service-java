package com.ddm.product.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.ddm.product.dto.response.TransactionDataResponseDto;
import com.ddm.product.entity.TransactionHistoryEntity;
import com.ddm.product.enums.TransactionTypeEnum;
import com.ddm.product.utils.TransactionInformationTestUtils;

@ExtendWith(SpringExtension.class)
public class TransactionHistoryDtoFromEntityMapperTest {

	@InjectMocks
	private TransactionHistoryDtoFromEntityMapper mapper;
	
	@Test
	void mapperTest() {		
		
		TransactionHistoryEntity entity1 = TransactionInformationTestUtils.formTransactionHistoryEntityWithCreditInfo();
		TransactionDataResponseDto response1 = mapper.apply(entity1);
		assertEquals(response1.getAmount(), BigDecimal.valueOf(100));
		assertEquals(response1.getDescription(), "Salary");
		assertEquals(response1.getId(), 1);
		assertEquals(response1.getTransactionDate(), LocalDate.of(2021, 1, 1));
		assertEquals(response1.getType(), TransactionTypeEnum.Income);
		assertNotNull(response1.getMoneyTransition());
		assertNull(response1.getMoneyTransition().getDebitedAccountAlias());
		assertNull(response1.getMoneyTransition().getDebitedAccountId());
		assertNull(response1.getMoneyTransition().getDebitedValue());
		assertEquals(response1.getMoneyTransition().getCreditedAccountId(), 1);
		assertEquals(response1.getMoneyTransition().getCreditedAccountAlias(), "Savings account");
		assertEquals(response1.getMoneyTransition().getCreditedValue(), BigDecimal.valueOf(6000));

		
		TransactionHistoryEntity entity2 = TransactionInformationTestUtils.formTransactionHistoryEntityWithDebitInfo();
		TransactionDataResponseDto response2 = mapper.apply(entity2);
		assertNotNull(response2.getMoneyTransition());
		assertNull(response2.getMoneyTransition().getCreditedAccountAlias());
		assertNull(response2.getMoneyTransition().getCreditedAccountId());
		assertNull(response2.getMoneyTransition().getCreditedValue());
		assertEquals(response2.getMoneyTransition().getDebitedAccountId(), 1);
		assertEquals(response2.getMoneyTransition().getDebitedAccountAlias(), "Savings account");
		assertEquals(response2.getMoneyTransition().getDebitedValue(), BigDecimal.valueOf(5500));
		
		TransactionHistoryEntity entity3 = TransactionInformationTestUtils.formTransactionHistoryEntityWithHistoryInfo();
		TransactionDataResponseDto response3 = mapper.apply(entity3);	
		assertEquals(response3.getMoneyTransition().getCreditedAccountId(), 2);
		assertEquals(response3.getMoneyTransition().getCreditedAccountAlias(), "Gold savings");
		assertEquals(response3.getMoneyTransition().getCreditedValue(), BigDecimal.valueOf(1000));
		assertEquals(response3.getMoneyTransition().getDebitedAccountId(), 1);
		assertEquals(response3.getMoneyTransition().getDebitedAccountAlias(), "Savings account");
		assertEquals(response3.getMoneyTransition().getDebitedValue(), BigDecimal.valueOf(1000));
		
	}
}
