package com.ddm.product.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.ddm.product.dto.response.AccountInfoResponseDto;
import com.ddm.product.entity.AccountInfoEntity;
import com.ddm.product.utils.AccountInformationTestUtils;

@ExtendWith(SpringExtension.class)
public class AccountInfoDtoFromEntityMapperTest {

	@InjectMocks
	private AccountInfoDtoFromEntityMapper accountInforDtoFromEntityMapper;
	
	@Test
	void mapperTest() {
		AccountInfoEntity entityValue = AccountInformationTestUtils.formSavingsAccountInfoEntity();
		AccountInfoResponseDto response = accountInforDtoFromEntityMapper
				.apply(entityValue);
		
		assertEquals(response.getId(), entityValue.getId());
		assertEquals(response.getAccountAlias(), entityValue.getAccountAlias());
		assertEquals(response.getAccountDescription(), entityValue.getAccountDescription());
		assertEquals(response.getAccountValue(), entityValue.getAccountValue());
	}
}
