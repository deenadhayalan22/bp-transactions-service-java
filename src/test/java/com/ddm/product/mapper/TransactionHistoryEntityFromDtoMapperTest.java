package com.ddm.product.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.ddm.product.dto.request.TransactionDataRequestDto;
import com.ddm.product.entity.TransactionHistoryEntity;
import com.ddm.product.enums.TransactionTypeEnum;
import com.ddm.product.utils.TransactionInformationTestUtils;

@ExtendWith(SpringExtension.class)
public class TransactionHistoryEntityFromDtoMapperTest {
	
	@InjectMocks
	private TransactionHistoryEntityFromDtoMapper mapper;
	
	@Test
	void mapperTest() {
		TransactionDataRequestDto request = TransactionInformationTestUtils.formIncomeTransactionDataAddRequestDto();
		TransactionHistoryEntity response = mapper.apply(request);
		
		assertEquals(BigDecimal.valueOf(5000), response.getAmount());
		assertEquals("Salary", response.getDescription());
		assertEquals(LocalDate.of(2021, 1, 1), response.getTransactionDate());
		assertEquals(TransactionTypeEnum.Income, response.getType());
	}

}
