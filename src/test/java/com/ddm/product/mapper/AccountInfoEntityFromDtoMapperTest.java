package com.ddm.product.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.ddm.product.dto.request.AccountInfoRequestDto;
import com.ddm.product.entity.AccountInfoEntity;
import com.ddm.product.utils.AccountInformationTestUtils;

@ExtendWith(SpringExtension.class)
public class AccountInfoEntityFromDtoMapperTest {
	
	@InjectMocks
	private AccountInfoEntityFromDtoMapper accountInfoEntityFromDtoMapper;
	
	@Test
	void mapperTest() {
		AccountInfoRequestDto requestDto = AccountInformationTestUtils.formAccoutInfoRequestDto();
		AccountInfoEntity response =  accountInfoEntityFromDtoMapper.apply(requestDto);
		
		assertEquals(requestDto.getAccountAlias(), response.getAccountAlias());
		assertEquals(requestDto.getAccountDescription(), response.getAccountDescription());
		assertEquals(requestDto.getAccountValue(), response.getAccountValue());
	}

}
