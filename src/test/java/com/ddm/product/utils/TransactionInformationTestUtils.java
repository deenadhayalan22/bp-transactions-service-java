package com.ddm.product.utils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.ddm.product.dto.request.TransactionDataRequestDto;
import com.ddm.product.dto.response.MoneyTransitionResponseDto;
import com.ddm.product.dto.response.TransactionDataResponseDto;
import com.ddm.product.entity.MoneyTransitionHistory;
import com.ddm.product.entity.TransactionHistoryEntity;
import com.ddm.product.enums.TransactionTypeEnum;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TransactionInformationTestUtils {
	
	public List<TransactionDataResponseDto> formTransactionDataResponseListDto() {
		return Arrays.asList(formIncomeTransactionDataResponseDto(), formExpenseTransactionDataResponseDto()
				, formInvestmentTransactionDataResponseDto());
	}
	
	public TransactionDataRequestDto formIncomeTransactionDataAddRequestDto() {
		return TransactionDataRequestDto.builder()
				.amount(BigDecimal.valueOf(5000)).description("Salary")
				.transactionDate(LocalDate.of(2021, 1, 1)).type(TransactionTypeEnum.Income)
				.creditedAccountId(1l).build();
	}
	
	public TransactionDataResponseDto formIncomeTransactionDataUpdateRequestDto() {
		return TransactionDataResponseDto.builder()
				.id(1l).amount(BigDecimal.valueOf(5000)).description("Salary")
				.transactionDate(LocalDate.of(2021, 1, 1)).type(TransactionTypeEnum.Income)
				.creditedAccountId(1l).build();
	}
	
	public TransactionDataResponseDto formTransactionDataUpdateInvalidRequestDto() {
		return TransactionDataResponseDto.builder().build();
	}
	
	public TransactionDataResponseDto formIncomeTransactionDataResponseDto() {
		return TransactionDataResponseDto.builder()
				.id(1l).amount(BigDecimal.valueOf(5000)).description("Salary")
				.transactionDate(LocalDate.of(2021, 1, 1)).type(TransactionTypeEnum.Income)
				.moneyTransition(formIncomeMoneyTransitionResponseDto()).build();
	}
	
	public MoneyTransitionResponseDto formIncomeMoneyTransitionResponseDto() {
		return MoneyTransitionResponseDto.builder()
				.creditedAccountId(1l).creditedAccountAlias("Savings accout")
				.creditedValue(BigDecimal.valueOf(6000)).build();
	}
	
	public TransactionDataResponseDto formExpenseTransactionDataResponseDto() {
		return TransactionDataResponseDto.builder()
				.id(1l).amount(BigDecimal.valueOf(100)).description("Food")
				.transactionDate(LocalDate.of(2021, 1, 2)).type(TransactionTypeEnum.Expense)
				.moneyTransition(formExpenseMoneyTransitionResponseDto()).build();
	}
	
	public MoneyTransitionResponseDto formExpenseMoneyTransitionResponseDto() {
		return MoneyTransitionResponseDto.builder()
				.debitedAccountId(1l).debitedAccountAlias("Savings accout")
				.debitedValue(BigDecimal.valueOf(5500)).build();
	}
	
	public TransactionDataResponseDto formInvestmentTransactionDataResponseDto() {
		return TransactionDataResponseDto.builder()
				.id(1l).amount(BigDecimal.valueOf(1000)).description("Purchase gold")
				.transactionDate(LocalDate.of(2021, 1, 1)).type(TransactionTypeEnum.Investments)
				.moneyTransition(formInvestmentMoneyTransitionResponseDto()).build();
	}
	
	public MoneyTransitionResponseDto formInvestmentMoneyTransitionResponseDto() {
		return MoneyTransitionResponseDto.builder()
				.debitedAccountId(1l).debitedAccountAlias("Savings accout")
				.debitedValue(BigDecimal.valueOf(4500))
				.creditedAccountId(2l).creditedAccountAlias("Gold account")
				.creditedValue(BigDecimal.valueOf(1000))
				.build();
	}
	
	public MoneyTransitionHistory formExpenseMoneyTransitionEntity() {
		return MoneyTransitionHistory.builder()
				.debitedAccountId(AccountInformationTestUtils.formSavingsAccountInfoEntity())
				.debitedAmount(BigDecimal.valueOf(5500)).build();
	}
	
	public TransactionHistoryEntity formTransactionHistoryEntityWithDebitInfo() {
		return TransactionHistoryEntity.builder().id(1l).description("Food").amount(BigDecimal.valueOf(100))
				.type(TransactionTypeEnum.Expense)
				.transactionDate(LocalDate.of(2021,1,1)).moneyTransitionHistory(formExpenseMoneyTransitionEntity())
				.build();
	}
	
	public MoneyTransitionHistory formIncomeMoneyTransitionEntity() {
		return MoneyTransitionHistory.builder()
				.creditedAccountId(AccountInformationTestUtils.formSavingsAccountInfoEntity())
				.creditedAmount(BigDecimal.valueOf(6000)).build();
	}
	
	public TransactionHistoryEntity formTransactionHistoryEntityWithCreditInfo() {
		return TransactionHistoryEntity.builder().id(1l).description("Salary").amount(BigDecimal.valueOf(100))
				.type(TransactionTypeEnum.Income)
				.transactionDate(LocalDate.of(2021,1,1)).moneyTransitionHistory(formIncomeMoneyTransitionEntity())
				.build();
	}
	
	public MoneyTransitionHistory formInvestmentMoneyTransitionEntity() {
		return MoneyTransitionHistory.builder()
				.debitedAccountId(AccountInformationTestUtils.formSavingsAccountInfoEntity())
				.creditedAmount(BigDecimal.valueOf(1000))
				.creditedAccountId(AccountInformationTestUtils.formInvestmentAccountInfoEntity())
				.debitedAmount(BigDecimal.valueOf(1000)).build();
	}
	
	public TransactionHistoryEntity formTransactionHistoryEntityWithHistoryInfo() {
		return TransactionHistoryEntity.builder().id(1l).description("Investments").amount(BigDecimal.valueOf(100))
				.type(TransactionTypeEnum.Investments)
				.transactionDate(LocalDate.of(2021,1,1)).moneyTransitionHistory(formInvestmentMoneyTransitionEntity())
				.build();
	}

}
