package com.ddm.product.utils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import com.ddm.product.dto.request.AccountInfoRequestDto;
import com.ddm.product.dto.response.AccountInfoResponseDto;
import com.ddm.product.entity.AccountInfoEntity;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AccountInformationTestUtils {
	
	public List<AccountInfoResponseDto> formAccoutInfoResonseDtoList() {
		return Arrays.asList(formAccoutInfoResponseDto());
	}	
	
	public AccountInfoResponseDto formAccoutInfoResponseDto() {
		return AccountInfoResponseDto.builder().id(1l).accountAlias("ABC bank").accountDescription("Savings account")
				.accountValue(BigDecimal.ONE).build();
	}
	
	public AccountInfoResponseDto formAccoutInfoInvalidResponseDto() {
		return AccountInfoResponseDto.builder().build();
	}
	
	public AccountInfoRequestDto formAccoutInfoRequestDto() {
		return AccountInfoRequestDto.builder().accountAlias("ABC bank").accountDescription("Savings account")
				.accountValue(BigDecimal.ONE).build();
	}
	
	public AccountInfoResponseDto formAccoutInfoUpdatedRequestDto() {
		return AccountInfoResponseDto.builder().id(1l).accountAlias("ABC bank").accountDescription("Savings account")
				.accountValue(BigDecimal.ONE).build();
	}
	
	public AccountInfoEntity formSavingsAccountInfoEntity() {
		return AccountInfoEntity.builder().id(1l).accountAlias("Savings account").accountDescription("General account")
				.accountValue(BigDecimal.valueOf(5000)).build();
	}
	
	public AccountInfoEntity formInvestmentAccountInfoEntity() {
		return AccountInfoEntity.builder().id(2l).accountAlias("Gold savings").accountDescription("Monthly gold purchase account")
				.accountValue(BigDecimal.valueOf(2000)).build();
	}

}
