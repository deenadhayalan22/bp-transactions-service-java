package com.ddm.product;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = BpTransactionServiceApplication.class)
class BpTransactionServiceApplicationTests {

	@Test
	void contextLoads() {
		assertTrue(true);
	}
}
