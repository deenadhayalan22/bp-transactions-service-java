package com.ddm.product.enums;

import java.util.EnumSet;

public enum TransactionTypeEnum {
	Expense, Income , Transfer, Savings, Investments;

	public static final EnumSet<TransactionTypeEnum> ADDITION_TYPE = EnumSet.of(Income);
	
	public static final EnumSet<TransactionTypeEnum> SUBSTRACT_TYPE = EnumSet.of(Expense, Transfer, Savings, Investments);
	
}
