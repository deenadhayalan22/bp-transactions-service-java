package com.ddm.product.dto;

import java.util.List;

import com.ddm.product.dto.request.AccountInfoRequestDto;
import com.ddm.product.dto.request.TransactionDataRequestDto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UtilityObject {
	
	private List<AccountInfoRequestDto> accountInfoList;
	
	private List<TransactionDataRequestDto> transList;

}
