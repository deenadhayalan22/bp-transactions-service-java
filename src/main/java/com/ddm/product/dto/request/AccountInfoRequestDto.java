package com.ddm.product.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.ddm.product.dto.response.AccountInfoResponseDto;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountInfoRequestDto {

	@NotNull
	private String accountAlias;

	@NotNull
	private String accountDescription;
	
	@NotNull
	private BigDecimal accountValue;

}
