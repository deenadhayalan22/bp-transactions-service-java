package com.ddm.product.dto.response;

import javax.validation.constraints.NotNull;

import com.ddm.product.dto.request.TransactionDataRequestDto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@NoArgsConstructor
public class TransactionDataResponseDto extends TransactionDataRequestDto {
	
	@NotNull
	private Long id;
	
	private MoneyTransitionResponseDto moneyTransition;

}
