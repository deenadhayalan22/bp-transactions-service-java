package com.ddm.product.dto.response;

import javax.validation.constraints.NotNull;

import com.ddm.product.dto.request.AccountInfoRequestDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountInfoResponseDto extends AccountInfoRequestDto {

	@NotNull
	private Long id;
}
