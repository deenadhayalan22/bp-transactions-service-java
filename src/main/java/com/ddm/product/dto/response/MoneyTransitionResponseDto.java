package com.ddm.product.dto.response;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoneyTransitionResponseDto {
	
	private Long debitedAccountId;
	
	private String debitedAccountAlias;
	
	private BigDecimal debitedValue;
	
	private Long creditedAccountId;
	
	private String creditedAccountAlias;
	
	private BigDecimal creditedValue;

}
