package com.ddm.product.service;

import java.util.List;

import com.ddm.product.dto.request.AccountInfoRequestDto;
import com.ddm.product.dto.response.AccountInfoResponseDto;

public interface AccountInfoService {
	
	AccountInfoResponseDto addAccountInformation(AccountInfoRequestDto accountInfoDto);
	
	Boolean addAccountInformationList(List<AccountInfoRequestDto> accountInfoDtoList);
	
	AccountInfoResponseDto updateAccountInformation(AccountInfoResponseDto accountInfoDto);
	
	Boolean deleteAccountInformation(Long accountInfoId);
	
	List<AccountInfoResponseDto> getAllAccountInformation();
	
	AccountInfoResponseDto getAccountInformationById(Long transactionId);

}
