package com.ddm.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddm.product.entity.AccountInfoEntity;
import com.ddm.product.exception.ResourceNotFoundException;
import com.ddm.product.repository.AccountInfoRepository;
import com.ddm.product.service.GenericCrudOperations;
import com.ddm.product.utils.SecurityUtils;

@Service
public class AccountInfoCrudOperations implements GenericCrudOperations<AccountInfoEntity> {
	
	@Autowired
	private AccountInfoRepository accountInfoRepository;

	@Override
	public AccountInfoEntity addOneRecord(AccountInfoEntity accountInfo) {
		return this.accountInfoRepository.save(accountInfo);
	}

	@Override
	public List<AccountInfoEntity> addAllRecords(List<AccountInfoEntity> accountInfoList) {
		return this.accountInfoRepository.saveAll(accountInfoList);
	}

	@Override
	public void deleteOneRecord(Long accountInfoId) {
		this.accountInfoRepository.deleteById(accountInfoId);
	}

	@Override
	public List<AccountInfoEntity> getAllRecords() {
		return this.accountInfoRepository.findByUserId(SecurityUtils.getUserId());
	}

	@Override
	public AccountInfoEntity getOneRecord(Long accountInfoId) {
		return this.accountInfoRepository.findById(accountInfoId)
				.orElseThrow(() -> new ResourceNotFoundException("AccountInfoEntity", "id", accountInfoId));
	}

}
