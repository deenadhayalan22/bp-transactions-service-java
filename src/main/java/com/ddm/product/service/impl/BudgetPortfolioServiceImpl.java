package com.ddm.product.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddm.product.entity.TransactionHistoryEntity;
import com.ddm.product.enums.TransactionTypeEnum;
import com.ddm.product.repository.TransactionHistoryRepo;
import com.ddm.product.service.BudgetPortfolioService;
import com.ddm.product.util.ApplicationUtils;
import com.ddm.product.utils.SecurityUtils;

@Service
public class BudgetPortfolioServiceImpl implements BudgetPortfolioService{
	
	@Autowired
	private TransactionHistoryRepo transactionHistoryRepo;

	@Override
	public Map<TransactionTypeEnum, Double> getMonthlyTransactionList(String period) {
		LocalDate formatedDate = ApplicationUtils.convertMonthYearFormatToLocalDate(period);
		LocalDate startDate = formatedDate.withDayOfMonth(1);
		LocalDate endDate = formatedDate.withDayOfMonth(startDate.lengthOfMonth());
		List<TransactionHistoryEntity> transList = 
				this.transactionHistoryRepo.findByUserIdAndTransactionDateBetween(SecurityUtils.getUserId(), startDate, endDate);
		
		return transList.stream()
				.collect(Collectors.groupingBy(TransactionHistoryEntity::getType, 
								Collectors.summingDouble(t -> t.getAmount().doubleValue())));
	}

}
