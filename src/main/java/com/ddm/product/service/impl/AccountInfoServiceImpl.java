package com.ddm.product.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddm.product.dto.request.AccountInfoRequestDto;
import com.ddm.product.dto.response.AccountInfoResponseDto;
import com.ddm.product.entity.AccountInfoEntity;
import com.ddm.product.mapper.AccountInfoDtoFromEntityMapper;
import com.ddm.product.mapper.AccountInfoEntityFromDtoMapper;
import com.ddm.product.service.AccountInfoService;
import com.ddm.product.service.GenericCrudOperations;

@Service
public class AccountInfoServiceImpl implements AccountInfoService {
	
	@Autowired
	private GenericCrudOperations<AccountInfoEntity> accountInfoCrudService;
	
	@Autowired
	private AccountInfoDtoFromEntityMapper accountInfoDtoMapper;
	
	@Autowired
	private AccountInfoEntityFromDtoMapper accountInfoEntityMapper;
	
	private void updateAccountInfoToEntity(AccountInfoEntity toAccount, AccountInfoRequestDto fromAccount) {
		toAccount.setAccountAlias(fromAccount.getAccountAlias());
		toAccount.setAccountDescription(fromAccount.getAccountDescription());
		toAccount.setAccountValue(fromAccount.getAccountValue());
	}
	
	@Override
	@Transactional
	public AccountInfoResponseDto addAccountInformation(AccountInfoRequestDto accountInfoDto) {
		AccountInfoEntity accountInfoEntity = accountInfoEntityMapper.apply(accountInfoDto);
		this.accountInfoCrudService.addOneRecord(accountInfoEntity);
		return this.accountInfoDtoMapper.apply(accountInfoEntity);
	}
	
	@Override
	@Transactional
	public Boolean addAccountInformationList(List<AccountInfoRequestDto> accountInfoDtoList) {
		List<AccountInfoEntity> accountInfoEntityList = accountInfoDtoList.stream().map(accountInfoEntityMapper)
				.collect(Collectors.toList());		
		this.accountInfoCrudService.addAllRecords(accountInfoEntityList);
		return true;		
	}

	@Override
	@Transactional
	public AccountInfoResponseDto updateAccountInformation(AccountInfoResponseDto accountInfoDto) {
		AccountInfoEntity accountInfoEntity = this.accountInfoCrudService.getOneRecord(accountInfoDto.getId());
		updateAccountInfoToEntity(accountInfoEntity, accountInfoDto);
		this.accountInfoCrudService.addOneRecord(accountInfoEntity);
		return this.accountInfoDtoMapper.apply(accountInfoEntity);
	}

	@Override
	@Transactional
	public Boolean deleteAccountInformation(Long accountInfoId) {
		this.accountInfoCrudService.deleteOneRecord(accountInfoId);
		return true;
	}

	@Override
	public List<AccountInfoResponseDto> getAllAccountInformation() {
		List<AccountInfoEntity> accountInfoList = this.accountInfoCrudService.getAllRecords();
		return accountInfoList.stream().map(accountInfoDtoMapper).collect(Collectors.toList());
	}

	@Override
	public AccountInfoResponseDto getAccountInformationById(Long transactionId) {
		AccountInfoEntity accoutInfoEntity = this.accountInfoCrudService.getOneRecord(transactionId);
		return this.accountInfoDtoMapper.apply(accoutInfoEntity);
	}

}
