package com.ddm.product.service.impl;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.ddm.product.dto.UtilityObject;
import com.ddm.product.repository.AccountInfoRepository;
import com.ddm.product.repository.TransactionHistoryRepo;
import com.ddm.product.service.AccountInfoService;
import com.ddm.product.service.TransactionService;
import com.ddm.product.service.UtilityService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UtilityServiceIpmpl implements UtilityService{
	
	@Autowired
	private AccountInfoService accountInfoService;
	
	@Autowired
	private TransactionService transService;
	
	@Autowired
	private AccountInfoRepository accountInfoRepository;
	
	@Autowired
	private TransactionHistoryRepo transHistoryRepository;

	@Override
	public boolean loadTestData() {
		// create Object Mapper
		ObjectMapper mapper = new ObjectMapper();

		// read JSON file and map/convert to java POJO
		try {
			this.transHistoryRepository.deleteAll();
			this.accountInfoRepository.deleteAll();
			UtilityObject testObj = mapper.readValue(ResourceUtils.getFile("classpath:testData.json"), UtilityObject.class);
			this.accountInfoService.addAccountInformationList(testObj.getAccountInfoList());
			this.transService.addTransactionList(testObj.getTransList());
			
		} catch (IOException e) {
		    e.printStackTrace();
		}
		return true;
	}

}
