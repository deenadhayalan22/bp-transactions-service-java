package com.ddm.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ddm.product.entity.TransactionHistoryEntity;
import com.ddm.product.exception.ResourceNotFoundException;
import com.ddm.product.repository.TransactionHistoryRepo;
import com.ddm.product.service.GenericCrudOperations;
import com.ddm.product.utils.SecurityUtils;

@Service
public class TransactionHistoryCrudOperations implements GenericCrudOperations<TransactionHistoryEntity>{
	
	@Autowired
	private TransactionHistoryRepo transactionHistoryRepo;

	@Override
	public TransactionHistoryEntity addOneRecord(TransactionHistoryEntity transHistory) {
		return this.transactionHistoryRepo.save(transHistory);
	}

	@Override
	public List<TransactionHistoryEntity> addAllRecords(List<TransactionHistoryEntity> transHistoryList) {
		return this.transactionHistoryRepo.saveAll(transHistoryList);
	}

	@Override
	public void deleteOneRecord(Long transHistoryId) {
		this.transactionHistoryRepo.deleteById(transHistoryId);		
	}

	@Override
	public List<TransactionHistoryEntity> getAllRecords() {
		return this.transactionHistoryRepo.findByUserId(SecurityUtils.getUserId());
	}

	@Override
	public TransactionHistoryEntity getOneRecord(Long transHistoryId) {
		return this.transactionHistoryRepo.findById(transHistoryId)
				.orElseThrow(() -> new ResourceNotFoundException("TransactionHistoryEntity", "id", transHistoryId));
	}

}
