package com.ddm.product.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.ddm.product.dto.request.TransactionDataRequestDto;
import com.ddm.product.dto.response.TransactionDataResponseDto;
import com.ddm.product.entity.AccountInfoEntity;
import com.ddm.product.entity.MoneyTransitionHistory;
import com.ddm.product.entity.TransactionHistoryEntity;
import com.ddm.product.mapper.TransactionHistoryDtoFromEntityMapper;
import com.ddm.product.mapper.TransactionHistoryEntityFromDtoMapper;
import com.ddm.product.service.GenericCrudOperations;
import com.ddm.product.service.TransactionService;

@Service
public class TransactionServiceImpl implements TransactionService {
	
	@Autowired
	private GenericCrudOperations<TransactionHistoryEntity> transHistoryCrudService;
	
	@Autowired
	private GenericCrudOperations<AccountInfoEntity> accountInfoCrudService;
	
	@Autowired
	private TransactionHistoryEntityFromDtoMapper transactionHistoryEntityMapper;
	
	@Autowired
	private TransactionHistoryDtoFromEntityMapper transactionHistoryResponseMapper;
	
	private void updateTransactionEntityFields(TransactionHistoryEntity toTrans, TransactionDataRequestDto fromTrans) {
		//Update transaction table
		toTrans.setAmount(fromTrans.getAmount());
		toTrans.setDescription(fromTrans.getDescription());
		toTrans.setTransactionDate(fromTrans.getTransactionDate());
		toTrans.setType(fromTrans.getType());
		
		//Update MoneyTransition table
		MoneyTransitionHistory moneyTransactionEntity = toTrans.getMoneyTransitionHistory();
		validateAndBuildMoneyTransactionHistory(moneyTransactionEntity, fromTrans);
		toTrans.setMoneyTransitionHistory(moneyTransactionEntity);
	}

	@Override
	@Transactional
	public TransactionDataResponseDto addTransaction(TransactionDataRequestDto transactionData) {
		//step 1 -- Add money transaction table
		MoneyTransitionHistory moneyTransitionHistory =  new MoneyTransitionHistory();
		validateAndBuildMoneyTransactionHistory(moneyTransitionHistory, transactionData);
		
		//step 2 -- Add account info to transaction table
		TransactionHistoryEntity trans = this.transactionHistoryEntityMapper.apply(transactionData);
		trans.setMoneyTransitionHistory(moneyTransitionHistory);
		
		this.transHistoryCrudService.addOneRecord(trans);	
		
		return this.transactionHistoryResponseMapper.apply(trans);
	}
	
	@Override
	@Transactional
	public Boolean addTransactionList(List<TransactionDataRequestDto> transactionDataList) {
		List<TransactionHistoryEntity> transactionHistoryEntityList =  new ArrayList<>();
		transactionDataList.forEach(transactionData ->  {
			//step 1 -- Add money transaction table
			MoneyTransitionHistory moneyTransitionHistory =  new MoneyTransitionHistory();
			validateAndBuildMoneyTransactionHistory(moneyTransitionHistory, transactionData);
			
			//step 2 -- Add account info to transaction table
			TransactionHistoryEntity trans = this.transactionHistoryEntityMapper.apply(transactionData);
			trans.setMoneyTransitionHistory(moneyTransitionHistory);
			
			transactionHistoryEntityList.add(trans);
		});		
		this.transHistoryCrudService.addAllRecords(transactionHistoryEntityList);
		return true;
	}
	
	public AccountInfoEntity updateAccountInfoEntity(Long accountId, BigDecimal amount, boolean isSum) {
		AccountInfoEntity accountInfoEntity = this.accountInfoCrudService.getOneRecord(accountId);
		
		if(isSum) {
			accountInfoEntity.setAccountValue(accountInfoEntity.getAccountValue().add(amount));
		}else {
			accountInfoEntity.setAccountValue(accountInfoEntity.getAccountValue().subtract(amount));
		}		
		return accountInfoEntity;
	}
	
	public void validateAndBuildMoneyTransactionHistory(MoneyTransitionHistory moneyTransitionHistory, TransactionDataRequestDto transactionData) {
		if(!ObjectUtils.isEmpty(transactionData.getDebitedAccountId())) {
			AccountInfoEntity debitedAccount = updateAccountInfoEntity(transactionData.getDebitedAccountId(), transactionData.getAmount(), false);		
			moneyTransitionHistory.setDebitedAccountId(debitedAccount);
			moneyTransitionHistory.setDebitedAmount(debitedAccount.getAccountValue());
		}
		
		if(!ObjectUtils.isEmpty(transactionData.getCreditedAccountId())) {
			AccountInfoEntity creditedAccount = updateAccountInfoEntity(transactionData.getCreditedAccountId(),transactionData.getAmount(), true);
			moneyTransitionHistory.setCreditedAccountId(creditedAccount);
			moneyTransitionHistory.setCreditedAmount(creditedAccount.getAccountValue());
		}
	}

	@Override
	@Transactional
	public TransactionDataResponseDto updateTransaction(TransactionDataResponseDto transactionData) {
		TransactionHistoryEntity trans = this.transHistoryCrudService.getOneRecord(transactionData.getId());
		updateTransactionEntityFields(trans, transactionData);
		this.transHistoryCrudService.addOneRecord(trans);
		return this.transactionHistoryResponseMapper.apply(trans);
	}

	@Override
	@Transactional
	public Boolean deleteTransaction(Long transactionId) {
		TransactionHistoryEntity trans = this.transHistoryCrudService.getOneRecord(transactionId);
		
		MoneyTransitionHistory moneyTransaction = trans.getMoneyTransitionHistory();
		List<AccountInfoEntity> accountInfoList = new ArrayList<>();
		
		if(!ObjectUtils.isEmpty(moneyTransaction.getDebitedAccountId())) {
			accountInfoList.add(updateAccountInfoEntity(moneyTransaction.getDebitedAccountId().getId(), trans.getAmount(), true));
		}
		
		if(!ObjectUtils.isEmpty(moneyTransaction.getCreditedAccountId())) {
			accountInfoList.add(updateAccountInfoEntity(moneyTransaction.getCreditedAccountId().getId(), trans.getAmount(), false));
		}
		
		if(ObjectUtils.isEmpty(accountInfoList)) {
			this.accountInfoCrudService.addAllRecords(accountInfoList);
		}
		
		this.transHistoryCrudService.deleteOneRecord(transactionId);
		return true;
	}

	@Override
	public List<TransactionDataResponseDto> getAllTransactions() {
		List<TransactionHistoryEntity> transList = this.transHistoryCrudService.getAllRecords();
		return transList.stream()
				.map(this.transactionHistoryResponseMapper)
				.sorted(Comparator.comparing(TransactionDataResponseDto::getId).reversed())
				.collect(Collectors.toList());
	}

	@Override
	public TransactionDataResponseDto getTransactionById(Long transactionId) {
		TransactionHistoryEntity transHistoryEntity =  this.transHistoryCrudService.getOneRecord(transactionId);
		return this.transactionHistoryResponseMapper.apply(transHistoryEntity);
	}

}
