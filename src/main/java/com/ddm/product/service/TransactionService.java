package com.ddm.product.service;

import java.util.List;

import com.ddm.product.dto.request.TransactionDataRequestDto;
import com.ddm.product.dto.response.TransactionDataResponseDto;

public interface TransactionService {
	
	TransactionDataResponseDto addTransaction(TransactionDataRequestDto transactionData);
	
	Boolean addTransactionList(List<TransactionDataRequestDto> transactionData);
	
	TransactionDataResponseDto updateTransaction(TransactionDataResponseDto transactionData);
	
	Boolean deleteTransaction(Long transactionId);
	
	List<TransactionDataResponseDto> getAllTransactions();
	
	TransactionDataResponseDto getTransactionById(Long transactionId);

}
