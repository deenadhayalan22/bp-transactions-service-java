package com.ddm.product.service;

import java.util.Map;

import com.ddm.product.enums.TransactionTypeEnum;

public interface BudgetPortfolioService {
	
	Map<TransactionTypeEnum, Double> getMonthlyTransactionList(String period);

}
