package com.ddm.product.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

import com.ddm.product.enums.TransactionTypeEnum;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ApplicationUtils {
	
	public final String MONTH_YEAR = "MMM-yyyy";
	
	public Double calculateValue(Double val1, Double val2, TransactionTypeEnum type) {
		if(TransactionTypeEnum.ADDITION_TYPE.contains(type)) {
			return val1+val2;
		}
		return val1-val2;
	}
	
	public LocalDate convertMonthYearFormatToLocalDate(String period) {
		DateTimeFormatter monthAndYearFormatter = new DateTimeFormatterBuilder().appendPattern(MONTH_YEAR)
				.parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
		        .toFormatter();;
		return LocalDate.parse(period, monthAndYearFormatter);
	}
}
