package com.ddm.product.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ddm.product.dto.ResponseTemplateDto;
import com.ddm.product.dto.request.AccountInfoRequestDto;
import com.ddm.product.dto.response.AccountInfoResponseDto;
import com.ddm.product.service.AccountInfoService;
import com.ddm.product.utils.ResponseBuilder;

@RestController
@RequestMapping("/account")
public class AccountInformationController {
	
	@Autowired
	private AccountInfoService accountInfoService;
	
	@GetMapping(path = "/detail")
	public ResponseEntity<ResponseTemplateDto<List<AccountInfoResponseDto>>> getAllAccountInformation() {
		List<AccountInfoResponseDto> accountInfoDtoList = this.accountInfoService.getAllAccountInformation();
		return ResponseBuilder.success(accountInfoDtoList);
	}
	
	@GetMapping(path = "/{accountInfoId}/detail")
	public ResponseEntity<ResponseTemplateDto<AccountInfoResponseDto>> getOneAccountInformation(
			@PathVariable("accountInfoId") Long accountInfoId) {
		AccountInfoResponseDto accountInfoDto = this.accountInfoService.getAccountInformationById(accountInfoId);
		return ResponseBuilder.success(accountInfoDto);
	}
	
	@PostMapping(path = "/detail")
	public ResponseEntity<ResponseTemplateDto<AccountInfoResponseDto>> addAccountInformation(
			@Valid @RequestBody final AccountInfoRequestDto accountInfoDto) {
		AccountInfoResponseDto response = this.accountInfoService.addAccountInformation(accountInfoDto);
		return ResponseBuilder.created(response);
	}
	
	@PutMapping(path = "/detail")
	public ResponseEntity<ResponseTemplateDto<AccountInfoResponseDto>> updateAccountInformation(
			@Valid @RequestBody final AccountInfoResponseDto accountInfoDto) {
		AccountInfoResponseDto response = this.accountInfoService.updateAccountInformation(accountInfoDto);
		return ResponseBuilder.success(response);
	}
	
	@DeleteMapping(path = "/{accountInfoId}/detail")
	public ResponseEntity<ResponseTemplateDto<Boolean>> deleteAccountInformation(
			@PathVariable("accountInfoId") Long accountInfoId) {
		Boolean isSuccess = this.accountInfoService.deleteAccountInformation(accountInfoId);
		return ResponseBuilder.success(isSuccess);		
	}

}
