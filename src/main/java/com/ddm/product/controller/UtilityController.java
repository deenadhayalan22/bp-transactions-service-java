package com.ddm.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ddm.product.dto.ResponseTemplateDto;
import com.ddm.product.service.UtilityService;
import com.ddm.product.utils.ResponseBuilder;

@RestController
@RequestMapping("/load/test/data")
public class UtilityController {
	
	@Autowired
	private UtilityService utilityService;
	
	@GetMapping
	public ResponseEntity<ResponseTemplateDto<Boolean>> loadTestData() {
		return ResponseBuilder.success(utilityService.loadTestData());
	}

}
