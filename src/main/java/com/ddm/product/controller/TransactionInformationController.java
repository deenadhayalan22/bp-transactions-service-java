package com.ddm.product.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ddm.product.dto.ResponseTemplateDto;
import com.ddm.product.dto.request.TransactionDataRequestDto;
import com.ddm.product.dto.response.TransactionDataResponseDto;
import com.ddm.product.service.TransactionService;
import com.ddm.product.utils.ResponseBuilder;

@RestController
@RequestMapping("/transaction")
public class TransactionInformationController {
	
	@Autowired
	private TransactionService transactionService;
	
	@GetMapping(path = "/record")
	public ResponseEntity<ResponseTemplateDto<List<TransactionDataResponseDto>>> getAllTransaction() {
		return ResponseBuilder.success(transactionService.getAllTransactions());
	}
	
	@GetMapping(path = "/{transactionId}/record")
	public ResponseEntity<ResponseTemplateDto<TransactionDataResponseDto>> getTransactionById(
			@PathVariable("transactionId") final Long transactionId) {
		return ResponseBuilder.success(transactionService.getTransactionById(transactionId));
	}
	
	@PostMapping(path = "/record")
	public ResponseEntity<ResponseTemplateDto<TransactionDataResponseDto>> addTransaction(
			@Valid @RequestBody final TransactionDataRequestDto transactionData) {
		return ResponseBuilder.created(transactionService.addTransaction(transactionData));
	}
	
	@PutMapping(path = "/record")
	public ResponseEntity<ResponseTemplateDto<TransactionDataResponseDto>> updateTransaction(
			@Valid @RequestBody final TransactionDataResponseDto transactionData) {
		return ResponseBuilder.success(transactionService.updateTransaction(transactionData));
	}
	
	@DeleteMapping(path = "/{transactionId}/record")
	public ResponseEntity<ResponseTemplateDto<Boolean>> deleteTransactionById(
			@PathVariable("transactionId") final Long transactionId) {
		return ResponseBuilder.success(transactionService.deleteTransaction(transactionId));
	}

}
