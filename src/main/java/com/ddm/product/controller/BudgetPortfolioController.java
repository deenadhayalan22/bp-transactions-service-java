package com.ddm.product.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ddm.product.dto.ResponseTemplateDto;
import com.ddm.product.enums.TransactionTypeEnum;
import com.ddm.product.service.BudgetPortfolioService;
import com.ddm.product.utils.ResponseBuilder;

@RestController
@RequestMapping("/budget")
public class BudgetPortfolioController {
	
	@Autowired
	private BudgetPortfolioService budgetPortfolioService;
	
	@GetMapping(path = "/{month}/portfolio")
	public ResponseEntity<ResponseTemplateDto<Map<TransactionTypeEnum, Double>>> getAllAccountInformation(
			@PathVariable("month") String period) {
		return ResponseBuilder.success(this.budgetPortfolioService.getMonthlyTransactionList(period));
	}

}
