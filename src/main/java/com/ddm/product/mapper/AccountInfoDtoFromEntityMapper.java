package com.ddm.product.mapper;

import java.util.function.Function;

import org.springframework.stereotype.Component;

import com.ddm.product.dto.response.AccountInfoResponseDto;
import com.ddm.product.entity.AccountInfoEntity;

@Component
public class AccountInfoDtoFromEntityMapper implements Function<AccountInfoEntity, AccountInfoResponseDto>{

	@Override
	public AccountInfoResponseDto apply(AccountInfoEntity accountInfoEntity) {
		return AccountInfoResponseDto.builder().id(accountInfoEntity.getId()).accountAlias(accountInfoEntity.getAccountAlias())
				.accountDescription(accountInfoEntity.getAccountDescription())
				.accountValue(accountInfoEntity.getAccountValue()).build();
	}
}
