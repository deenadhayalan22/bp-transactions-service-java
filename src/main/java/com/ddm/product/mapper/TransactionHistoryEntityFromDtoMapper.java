package com.ddm.product.mapper;

import java.util.function.Function;

import org.springframework.stereotype.Component;

import com.ddm.product.dto.request.TransactionDataRequestDto;
import com.ddm.product.entity.TransactionHistoryEntity;

@Component
public class TransactionHistoryEntityFromDtoMapper implements Function<TransactionDataRequestDto, TransactionHistoryEntity>{

	@Override
	public TransactionHistoryEntity apply(TransactionDataRequestDto transactionData) {
		return TransactionHistoryEntity.builder().amount(transactionData.getAmount())
				.description(transactionData.getDescription()).transactionDate(transactionData.getTransactionDate())
				.type(transactionData.getType()).build();
	}

}
