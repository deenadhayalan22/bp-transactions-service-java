package com.ddm.product.mapper;

import java.util.function.Function;

import org.springframework.stereotype.Component;

import com.ddm.product.dto.request.AccountInfoRequestDto;
import com.ddm.product.entity.AccountInfoEntity;

@Component
public class AccountInfoEntityFromDtoMapper implements Function<AccountInfoRequestDto, AccountInfoEntity>{

	@Override
	public AccountInfoEntity apply(AccountInfoRequestDto accountInfoDto) {
		return AccountInfoEntity.builder().accountAlias(accountInfoDto.getAccountAlias())
				.accountDescription(accountInfoDto.getAccountDescription())
				.accountValue(accountInfoDto.getAccountValue())
				.build();
	}

}
