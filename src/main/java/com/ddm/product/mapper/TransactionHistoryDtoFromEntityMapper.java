package com.ddm.product.mapper;

import java.util.function.Function;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.ddm.product.dto.response.MoneyTransitionResponseDto;
import com.ddm.product.dto.response.TransactionDataResponseDto;
import com.ddm.product.entity.TransactionHistoryEntity;

@Component
public class TransactionHistoryDtoFromEntityMapper implements Function<TransactionHistoryEntity, TransactionDataResponseDto>{

	@Override
	public TransactionDataResponseDto apply(TransactionHistoryEntity transactionEntity) {
		MoneyTransitionResponseDto moneyTransitionDto = new MoneyTransitionResponseDto();
		if(!ObjectUtils.isEmpty(transactionEntity.getMoneyTransitionHistory().getDebitedAccountId())) {
			moneyTransitionDto.setDebitedAccountId(transactionEntity.getMoneyTransitionHistory().getDebitedAccountId().getId());
			moneyTransitionDto.setDebitedAccountAlias(transactionEntity.getMoneyTransitionHistory().getDebitedAccountId().getAccountAlias());
			moneyTransitionDto.setDebitedValue(transactionEntity.getMoneyTransitionHistory().getDebitedAmount());
		}
		
		if(!ObjectUtils.isEmpty(transactionEntity.getMoneyTransitionHistory().getCreditedAccountId())) {
			moneyTransitionDto.setCreditedAccountId(transactionEntity.getMoneyTransitionHistory().getCreditedAccountId().getId());
			moneyTransitionDto.setCreditedAccountAlias(transactionEntity.getMoneyTransitionHistory().getCreditedAccountId().getAccountAlias());
			moneyTransitionDto.setCreditedValue(transactionEntity.getMoneyTransitionHistory().getCreditedAmount());
		}
		
		return TransactionDataResponseDto.builder()
				.amount(transactionEntity.getAmount())
				.description(transactionEntity.getDescription())
				.id(transactionEntity.getId())
				.transactionDate(transactionEntity.getTransactionDate())
				.type(transactionEntity.getType())		
				.moneyTransition(moneyTransitionDto)
				.build();
	}

}
