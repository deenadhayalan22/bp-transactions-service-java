package com.ddm.product.entity;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ddm.product.audit.Auditable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@Table(name = "AccountDetailInfo")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class AccountInfoEntity extends Auditable<UUID>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "account_alias")
	private String accountAlias;
	
	@Column(name = "account_description")
	private String accountDescription;
	
	@Column(name = "account_value")
	private BigDecimal accountValue;
}
