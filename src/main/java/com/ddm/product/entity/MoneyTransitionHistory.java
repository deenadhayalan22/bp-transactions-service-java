package com.ddm.product.entity;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ddm.product.audit.Auditable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@Table(name = "MoneyTransitionHistory")
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class MoneyTransitionHistory extends Auditable<UUID>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "debited_account_id")
	private AccountInfoEntity debitedAccountId;
	
	private BigDecimal debitedAmount;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "credited_account_Id")
	private AccountInfoEntity creditedAccountId;
	
	private BigDecimal creditedAmount;

}
