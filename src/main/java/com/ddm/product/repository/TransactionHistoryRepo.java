package com.ddm.product.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ddm.product.entity.TransactionHistoryEntity;

@Repository
public interface TransactionHistoryRepo extends JpaRepository<TransactionHistoryEntity, Long> {
	
	List<TransactionHistoryEntity> findByUserId(UUID userId);
	
	@Query(name = "SELECT T FROM TransactionHistoryEntity WHERE userId=:userId "
			+ "AND transactionDate BETWEEN :startDate AND :endDate")
	List<TransactionHistoryEntity> findByUserIdAndTransactionDateBetween(@Param("userId") UUID userId, 
			@Param("startDate") LocalDate startDate, @Param("endDate")  LocalDate endDate);

}
