package com.ddm.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ddm.product.entity.MoneyTransitionHistory;

@Repository
public interface MoneyTransactionInfoRepository extends JpaRepository<MoneyTransitionHistory, Long>{

}
