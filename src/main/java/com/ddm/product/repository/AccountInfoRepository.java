package com.ddm.product.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ddm.product.entity.AccountInfoEntity;

@Repository
public interface AccountInfoRepository extends JpaRepository<AccountInfoEntity, Long>{
	
	List<AccountInfoEntity> findByUserId(UUID userId);

}
