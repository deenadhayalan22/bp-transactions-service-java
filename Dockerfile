FROM openjdk:11
EXPOSE 8080
ADD target/my-pocket-0.0.1-SNAPSHOT.jar my-pocket-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/my-pocket-0.0.1-SNAPSHOT.jar"] 